import { Component, OnInit, ViewChild } from '@angular/core';
import * as XLS from 'xlsx';
import { HttpClient } from '@angular/common/http';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;

  
  title = 'boletin-empleo';
  data:any;

  registradosJson:any;
  orientadosJson:any;
  formadosBtJson:any;
  formadosLJson:any;
  remitidosJson:any;
  colocadosJson:any;

  jsonSeleccionado = [];
  tablaseleccionada = "registrados";
  titulos = [""];
  datosTotal = new Array();



  constructor(
    private http:HttpClient
  ) {

    this.getConfig();

   }


   ngOnInit() {
  }

  configUrl = 'assets/xls/Bases Consolidadas_APED.xlsx';

  getConfig() {
    this.http.get(this.configUrl, { responseType: 'blob' }).subscribe((data:any)=>{
      const reader: FileReader = new FileReader();

      

      reader.onload = (e: any) => {
        const bstr: string = e.target.result;
        const wb: XLS.WorkBook = XLS.read(bstr, { type: 'binary' });

        /* grab first sheet */
        const wsname1: string = wb.SheetNames[0];
        const ws1: XLS.WorkSheet = wb.Sheets[wsname1];

        /* grab second sheet */
        const wsname2: string = wb.SheetNames[1];
        const ws2: XLS.WorkSheet = wb.Sheets[wsname2];

        /* save data */
        this.registradosJson = XLS.utils.sheet_to_json(ws1);
        this.orientadosJson = XLS.utils.sheet_to_json(ws2);
        console.log("registradosJson",this.registradosJson);
        console.log("orientadosJson",this.orientadosJson);

        

      };
      reader.readAsBinaryString(data);
      console.log("BloB", data);
    });
  }

  getDataTotal(fila:number){
    const filas = new Array();
    filas.push(this.jsonSeleccionado[fila]['__EMPTY']);
    for(var i = 1; i < 14; i ++){
      filas.push(this.jsonSeleccionado[fila]['__EMPTY_'+i]);
    }
    this.datosTotal.push(filas);
  }

  getDatos(datos:any){
    datos.forEach((element:any) => {
      const filas = new Array();
      filas.push(element['__EMPTY']);
      for(var i = 1; i < 14; i ++){
        filas.push(element['__EMPTY_'+i]);
      }
      this.datosTotal.push(filas);
    });
  }

  getTitle(fila:number){
    for(var i = 1; i < 14; i ++){
      this.titulos.push(this.jsonSeleccionado[fila]['__EMPTY_'+i]);
    }
  }

  change(link:any, desde:number, hasta:number){
    this.limpiar();
    this.tablaseleccionada = link;
    this.jsonSeleccionado = this.registradosJson;
    this.getTitle(0);
    if(desde == hasta){
      this.getDataTotal(desde);
    }
    else{
      const datos = this.jsonSeleccionado.filter(data=>{
        return data['Base Registrados_Enero - Noviembre 2022'] == link;
      });
      this.getDatos(datos);
      console.log("DATOS", this.datosTotal);
    }   
    
    
  }

  limpiar(){
    this.jsonSeleccionado = [];
    this.tablaseleccionada = "";
    this.titulos = [""];
    this.datosTotal = [];
  }

  
}
